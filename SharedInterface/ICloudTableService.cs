﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedInterface
{
    public interface ICloudTableService<T>
    {
        Task<T> InsertAsync(T item);
        Task<T> ReadSingleAsync(string id);
        Task<IEnumerable<T>> QueryAsync(IQueryable<T> query);
        Task<T> UpdateAsync(T item);
        Task DeleteAsync(T item);

        Task<ICollection<T>> ReadAllAsync();
    }
}