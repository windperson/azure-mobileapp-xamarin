﻿using System;

namespace SharedInterface
{
    public interface IClientSyncable
    {
        string Id { get; set; }

        DateTimeOffset? CreatedAt { get; set; }

        DateTimeOffset? UpdatedAt { get; set; }

        byte[] Version { get; set; }
    }
}