﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SharedInterface.TodoItem
{
    public interface ITodoItemService<T> : ICloudTableService<T> where T : ITodoItem
    {
        //Add new Custom Method declaration if needed.
        Task<IEnumerable<T>> GetCompletedItems();
    }
}
