﻿namespace SharedInterface.TodoItem
{
    public interface ITodoItem
    {
        string Text { get; set; }

        bool Complete { get; set; }
    }
}
