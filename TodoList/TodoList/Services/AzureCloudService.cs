﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using SharedInterface;
using TodoList.Abstractions;
using TodoList.Handler;
using TodoList.Models;

namespace TodoList.Services
{
    public class AzureCloudService : ICloudService
    {
        public MobileServiceClient MobileServiceClient { get; }
        public TodoItemTable TodoItemTable { get; }

        public AzureCloudService(string serverUrl)
        {
#if DEBUG   
            MobileServiceClient = new MobileServiceClient(serverUrl, new LoggingHandler(true));
#else
            MobileServiceClient = new MobileServiceClient(serverUrl);
#endif
            TodoItemTable = new TodoItemTable(MobileServiceClient);
        }

       
    }
}
