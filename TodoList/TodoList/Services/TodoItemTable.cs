﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using SharedInterface;
using SharedInterface.TodoItem;
using TodoList.Abstractions;
using TodoList.Models;

namespace TodoList.Services
{
    public class TodoItemTable : AbstractAzureCloudTable<TodoItem>, ITodoItemService<TodoItem>
    {        
        public TodoItemTable(MobileServiceClient client) : base(client)
        {
        }

        public Task<IEnumerable<TodoItem>> GetCompletedItems()
        {
            return Table.Where(item => item.Complete).OrderBy(item => item.UpdatedAt).ToEnumerableAsync();
        }
    }
}