﻿using SharedInterface;
using SharedInterface.TodoItem;
using TodoList.Abstractions;

namespace TodoList.Models
{
    public class TodoItem : AbstractClientSyncableData, ITodoItem
    {
        public string Text { get; set; }
        public bool Complete { get; set; }
    }
}