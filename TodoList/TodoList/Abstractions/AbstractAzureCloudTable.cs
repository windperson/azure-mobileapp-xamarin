﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using SharedInterface;

namespace TodoList.Abstractions
{
    public abstract class AbstractAzureCloudTable<T> : ICloudTableService<T> where T : IClientSyncable
    {
        protected readonly MobileServiceClient Client;

        protected readonly IMobileServiceTable<T> Table;

        protected AbstractAzureCloudTable(MobileServiceClient client)
        {
            Client = client;
            Table = client.GetTable<T>();
        }
        public IMobileServiceTableQuery<T> GeTableQuery()
        {
            return Table.CreateQuery();
        }

        #region CRUD Interface Implementation

        public async Task<T> InsertAsync(T item)
        {
            await Table.InsertAsync(item);
            return item;
        }

        public async Task<T> ReadSingleAsync(string id)
        {
            return await Table.LookupAsync(id);
        }

        public Task<IEnumerable<T>> QueryAsync(IQueryable<T> query)
        {
            var tableQuery = Table.CreateQuery();
            tableQuery.Query = query;
            return tableQuery.ToEnumerableAsync();
        }

        public async Task DeleteAsync(T item)
        {
            await Table.DeleteAsync(item);
        }

        public async Task<T> UpdateAsync(T item)
        {
            await Table.UpdateAsync(item);
            return item;
        }

        public async Task<ICollection<T>> ReadAllAsync()
        {
            return await Table.ToListAsync();
        }

        #endregion
    }
}