﻿using Microsoft.WindowsAzure.MobileServices;
using SharedInterface;

namespace TodoList.Abstractions
{
    public interface ICloudService
    {
        MobileServiceClient MobileServiceClient { get; }
    }
}