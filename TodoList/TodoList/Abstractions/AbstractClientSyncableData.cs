﻿using System;
using SharedInterface;

namespace TodoList.Abstractions
{
    public abstract class AbstractClientSyncableData : IClientSyncable
    {
        public string Id { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        public byte[] Version { get; set; }
    }
}