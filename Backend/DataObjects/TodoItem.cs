﻿using Microsoft.Azure.Mobile.Server;
using SharedInterface.TodoItem;

namespace Backend.DataObjects
{
    public class TodoItem : EntityData, ITodoItem
    {
        public string Text { get; set; }

        public bool Complete { get; set; }
    }
}